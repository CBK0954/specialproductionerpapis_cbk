package shop.dev.choibk.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0, "성공하였습니다."),
    FAILED(-1, "실패하였습니다."),

    ACCESS_DENIED(-1000, "권한이 없습니다."),
    USERNAME_SIGN_IN_FAILED(-1001, "가입된 사용자가 아닙니다."),
    AUTHENTICATION_ENTRY_POINT(-1002, "접근 권한이 없습니다."),

    MISSING_DATA(-10000, "데이터를 찾을 수 없습니다."),
    WRONG_PHONE_NUMBER(-10001, "잘못된 핸드폰 번호입니다."),

    TOO_LONG_USERNAME(-100, "아이디는 5 ~ 20자로 생성 가능합니다."),
    DUPLICATION_USERNAME(-101, "중복된 아이디 입니다."),
    WRONG_PASSWORD(-102, "암호 불일치 또는 암호 길이 형식(8 ~ 20자)이 맞지 않거나 ID 값에 오류가 발생하였습니다."),

    NOT_VALID_USERNAME_TYPE(-103, "유효한 아이디 형식이 아닙니다.");

    private final Integer code;
    private final String msg;
}
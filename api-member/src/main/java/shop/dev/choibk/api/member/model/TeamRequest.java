package shop.dev.choibk.api.member.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import shop.dev.choibk.api.member.enums.MemberGroup;

import java.time.LocalDateTime;

@Getter
@Setter
public class TeamRequest {
    @ApiModelProperty(value = "팀 명", required = true)
    private String teamName;

    @ApiModelProperty(value = "주 업무", required = true)
    private String mainWork;

    @ApiModelProperty(value = "권한", required = true)
    private MemberGroup memberGroup;

    @ApiModelProperty(value = "활성화 여부", required = true)
    private Boolean isUse;

    @ApiModelProperty(value = "등록 일자", required = true)
    private LocalDateTime createDate;
}
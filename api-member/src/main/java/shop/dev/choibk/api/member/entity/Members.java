package shop.dev.choibk.api.member.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.dev.choibk.api.member.enums.MemberGroup;
import shop.dev.choibk.api.member.model.MembersRequest;
import shop.dev.choibk.api.member.model.MembersUpdateRequest;
import shop.dev.choibk.common.interfaces.CommonModelBuilder;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor
public class Members {
    @ApiModelProperty("시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty("사용자 ID")
    @Column(nullable = false)
    private String username; // 사용자 ID

    @ApiModelProperty("사용자 PW")
    @Column(nullable = false)
    private String password; // 사용자 PW

    @ApiModelProperty("활성화 여부")
    @Column(nullable = false)
    private Boolean isUse; // 활성화 여부

    @ApiModelProperty("팀명")
    @Column(nullable = false)
    private String teamName; // 팀명 (A팀, B팀..)

    @ApiModelProperty("권한")
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private MemberGroup memberGroup; // 권한 (관리자, 팀장, 사원 등)

    private Members(MemberBuilder builder) {
        this.username = builder.username;
        this.password = builder.password;
        this.isUse = builder.isUse;
        this.teamName = builder.teamName;
        this.memberGroup = builder.memberGroup;
    }

    public void putMember(MembersUpdateRequest request) {
        this.isUse = request.getIsUse();
    }

    public static class MemberBuilder implements CommonModelBuilder<Members> {
        private final String username;
        private final String password;
        private final Boolean isUse;
        private final String teamName;
        private final MemberGroup memberGroup;

        public MemberBuilder(MembersRequest request) {
            this.username = request.getUsername();
            this.password = request.getPassword();
            this.isUse = request.getIsUse();
            this.teamName = request.getTeamName();
            this.memberGroup = request.getMemberGroup();
        }

        @Override
        public Members build() {
            return new Members(this);
        }
    }
}
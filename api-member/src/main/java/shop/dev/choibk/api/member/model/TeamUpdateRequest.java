package shop.dev.choibk.api.member.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class TeamUpdateRequest {
    @NotNull
    private Boolean isUse;
}
package shop.dev.choibk.api.member.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.dev.choibk.api.member.entity.Members;
import shop.dev.choibk.api.member.model.MembersDetails;
import shop.dev.choibk.api.member.model.MembersRequest;
import shop.dev.choibk.api.member.model.MembersUpdateRequest;
import shop.dev.choibk.api.member.repository.MembersRepository;
import shop.dev.choibk.common.response.model.ListResult;
import shop.dev.choibk.common.response.service.ListConvertService;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MembersService {
    private final MembersRepository membersRepository;

    public void setMember(MembersRequest request) {
        Members addData = new Members.MemberBuilder(request).build();

        membersRepository.save(addData);
    }

    public ListResult<MembersDetails> getMembers() {
        List<Members> originList = membersRepository.findAll();
        List<MembersDetails> result = new ArrayList<>();
        originList.forEach(item -> result.add(new MembersDetails.MemberDetailsBuilder(item).build()));
        return ListConvertService.settingResult(result);
    }

    public void putBoard(long id, MembersUpdateRequest request) {
        Members originData = membersRepository.findById(id).orElseThrow(ClassCastException::new);
        originData.putMember(request);

        membersRepository.save(originData);
    }
}
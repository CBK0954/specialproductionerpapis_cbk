package shop.dev.choibk.api.member.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.dev.choibk.api.member.entity.Team;

public interface TeamRepository extends JpaRepository<Team, Long> {
}
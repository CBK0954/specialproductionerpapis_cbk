package shop.dev.choibk.api.member.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.dev.choibk.api.member.entity.Members;
import shop.dev.choibk.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MembersItem {
    private Long id;
    private String username;
    private String password;
    private Boolean isUse;

    private MembersItem(MemberItemBuilder builder) {
        this.username = builder.username;
        this.password = builder.password;
        this.isUse = builder.isUse;
    }
    public static class MemberItemBuilder implements CommonModelBuilder<MembersItem> {
        private final String username;
        private final String password;
        private final Boolean isUse;

        public MemberItemBuilder(Members members) {
            this.username = members.getUsername();
            this.password = members.getPassword();
            this.isUse = members.getIsUse();
        }

        @Override
        public MembersItem build() {
            return new MembersItem(this);
        }
    }
}
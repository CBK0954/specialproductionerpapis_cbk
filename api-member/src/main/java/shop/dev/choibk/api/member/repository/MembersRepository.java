package shop.dev.choibk.api.member.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.dev.choibk.api.member.entity.Members;

public interface MembersRepository extends JpaRepository<Members, Long> {
}
package shop.dev.choibk.api.member.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MembersUpdateRequest {
    @NotNull
    private Boolean isUse;
}

package shop.dev.choibk.api.member.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import shop.dev.choibk.api.member.enums.MemberGroup;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MembersRequest {
    @ApiModelProperty(value = "아이디", required = true)
    @NotNull
    private String username;

    @ApiModelProperty(value = "비밀번호", required = true)
    private String password;

    @ApiModelProperty(value = "권한", required = true)
    private MemberGroup memberGroup;

    @ApiModelProperty(value = "활성화 여부", required = true)
    private Boolean isUse;

    @ApiModelProperty(value = "팀 이름", required = true)
    private String teamName;
}
package shop.dev.choibk.api.member.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.dev.choibk.api.member.entity.Members;
import shop.dev.choibk.api.member.enums.MemberGroup;
import shop.dev.choibk.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MembersDetails {
    private String username;
    private String teamName;
    private MemberGroup memberGroup;

    private MembersDetails(MemberDetailsBuilder builder) {
        this.username = builder.username;
        this.teamName = builder.teamName;
        this.memberGroup = builder.memberGroup;
    }

    public static class MemberDetailsBuilder implements CommonModelBuilder<MembersDetails> {
        private final String username;
        private final String teamName;
        private final MemberGroup memberGroup;

        public MemberDetailsBuilder(Members members) {
            this.username = members.getUsername();
            this.teamName = members.getTeamName();
            this.memberGroup = members.getMemberGroup();
        }
        @Override
        public MembersDetails build() {
            return new MembersDetails(this);
        }
    }
}
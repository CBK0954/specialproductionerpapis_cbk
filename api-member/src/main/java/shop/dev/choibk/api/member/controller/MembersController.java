package shop.dev.choibk.api.member.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import shop.dev.choibk.api.member.model.MembersRequest;
import shop.dev.choibk.api.member.model.MembersUpdateRequest;
import shop.dev.choibk.api.member.service.MembersService;
import shop.dev.choibk.common.response.model.CommonResult;
import shop.dev.choibk.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "사용자 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")

public class MembersController {
    private final MembersService membersService;

    @ApiOperation(value = "사용자 등록")
    @PostMapping("/register")
    public CommonResult setMember(@RequestBody @Valid MembersRequest request) {
        membersService.setMember(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "사용자 상세정보")
    @GetMapping("/list")
    public CommonResult getMembers() {
        return ResponseService.getListResult(membersService.getMembers(),true);
    }

    @ApiOperation(value = "사용자 수정")
    @PutMapping("/correction/{id}")
    public CommonResult putMember(@PathVariable long id, @RequestBody @Valid MembersUpdateRequest request) {
        membersService.putBoard(id, request);
        return ResponseService.getSuccessResult();
    }
}

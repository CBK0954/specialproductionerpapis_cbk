package shop.dev.choibk.api.member.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.dev.choibk.api.member.enums.MemberGroup;
import shop.dev.choibk.api.member.model.TeamRequest;
import shop.dev.choibk.api.member.model.TeamUpdateRequest;
import shop.dev.choibk.common.interfaces.CommonModelBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor
public class Team {
    @ApiModelProperty("시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty("팀명")
    @Column(nullable = false)
    private String teamName; // 팀명

    @ApiModelProperty("주 업무")
    @Column(nullable = false)
    private String mainWork; // 주 업무

    @ApiModelProperty("권한")
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private MemberGroup memberGroup; // 권한 (관리자, 팀장, 사원 등)

    @ApiModelProperty("활성화 여부")
    @Column(nullable = false)
    private Boolean isUse; // 활성화 여부

    @ApiModelProperty("등록 일자")
    @Column(nullable = false)
    private LocalDateTime createDate; // 등록 일자

    private Team(TeamBuilder builder) {
        this.teamName = builder.teamName;
        this.mainWork = builder.mainWork;
        this.memberGroup = builder.memberGroup;
        this.isUse = builder.isUse;
        this.createDate = builder.createDate;
    }

    public void putTeam(TeamUpdateRequest request) {
        this.isUse = request.getIsUse();
    }

    public static class TeamBuilder implements CommonModelBuilder<Team> {
        private final String teamName;
        private final String mainWork;
        private final MemberGroup memberGroup;
        private final Boolean isUse;
        private final LocalDateTime createDate;

        public TeamBuilder(TeamRequest request) {
            this.teamName = request.getTeamName();
            this.mainWork = request.getMainWork();
            this.memberGroup = request.getMemberGroup();
            this.isUse = request.getIsUse();
            this.createDate = request.getCreateDate();
        }

        @Override
        public Team build() {
            return new Team(this);
        }
    }
}
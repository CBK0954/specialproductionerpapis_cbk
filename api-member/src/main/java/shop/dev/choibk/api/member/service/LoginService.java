package shop.dev.choibk.api.member.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import shop.dev.choibk.api.member.configure.JwtTokenProvider;
import shop.dev.choibk.api.member.entity.Member;
import shop.dev.choibk.api.member.model.LoginRequest;
import shop.dev.choibk.api.member.model.LoginResponse;
import shop.dev.choibk.api.member.repository.MemberRepository;
import shop.dev.choibk.common.enums.MemberGroup;
import shop.dev.choibk.common.exception.CMissingDataException;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;

    public LoginResponse doLogin(MemberGroup memberGroup, LoginRequest loginRequest, String loginType) {
        Member member = memberRepository.findByUsername(loginRequest.getUsername()).orElseThrow(CMissingDataException::new); // 회원정보가 없습니다. 던지기

        if (!member.getIsEnabled()) throw new CMissingDataException(); // 회원정보가 없습니다. 던지기
        if (!member.getMemberGroup().equals(memberGroup)) throw new CMissingDataException(); // 회원정보가 없습니다. 던지기
        if (!passwordEncoder.matches(loginRequest.getPassword(), member.getPassword())) throw new CMissingDataException(); // 회원정보가 없습니다. 던지기

        String token = jwtTokenProvider.createToken(member.getUsername(), member.getMemberGroup().toString(), loginType);

        return new LoginResponse.LoginResponseBuilder(token, member.getName()).build();
    }
}
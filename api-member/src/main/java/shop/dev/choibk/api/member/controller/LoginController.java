package shop.dev.choibk.api.member.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shop.dev.choibk.api.member.model.LoginRequest;
import shop.dev.choibk.api.member.model.LoginResponse;
import shop.dev.choibk.api.member.service.LoginService;
import shop.dev.choibk.common.enums.MemberGroup;
import shop.dev.choibk.common.response.model.SingleResult;
import shop.dev.choibk.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "로그인")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member/login")
public class LoginController {
    private final LoginService loginService;

    @ApiOperation(value = "관리자 로그인")
    @PostMapping("/admin")
    public SingleResult<LoginResponse> doLoginAdminWeb(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_ADMIN, loginRequest, "WEB"));
    }
}
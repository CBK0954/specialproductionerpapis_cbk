package shop.dev.choibk.api.member.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.dev.choibk.api.member.entity.Team;
import shop.dev.choibk.api.member.enums.MemberGroup;
import shop.dev.choibk.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TeamItem {
    private Long id;
    private String teamName;
    private String mainWork;
    private MemberGroup memberGroup;
    private Boolean isUse;

    private TeamItem(TeamItemBuilder builder) {
        this.id = builder.id;
        this.teamName = builder.teamName;
        this.mainWork = builder.mainWork;
        this.memberGroup = builder.memberGroup;
        this.isUse = builder.isUse;
    }

    public static class TeamItemBuilder implements CommonModelBuilder<TeamItem> {
        private final Long id;
        private final String teamName;
        private final String mainWork;
        private final MemberGroup memberGroup;
        private final Boolean isUse;

        public TeamItemBuilder(Team team) {
            this.id = team.getId();
            this.teamName = team.getTeamName();
            this.mainWork = team.getMainWork();
            this.memberGroup = team.getMemberGroup();
            this.isUse = team.getIsUse();
        }
        @Override
        public TeamItem build() {
            return new TeamItem(this);
        }
    }
}
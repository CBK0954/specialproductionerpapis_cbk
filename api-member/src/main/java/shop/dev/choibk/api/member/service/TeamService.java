package shop.dev.choibk.api.member.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.dev.choibk.api.member.entity.Team;
import shop.dev.choibk.api.member.model.TeamItem;
import shop.dev.choibk.api.member.model.TeamRequest;
import shop.dev.choibk.api.member.model.TeamUpdateRequest;
import shop.dev.choibk.api.member.repository.TeamRepository;
import shop.dev.choibk.common.response.model.ListResult;
import shop.dev.choibk.common.response.service.ListConvertService;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TeamService {
    private final TeamRepository teamRepository;

    public void setTeam(TeamRequest request) {
        Team addData = new Team.TeamBuilder(request).build();

        teamRepository.save(addData);
    }

    public ListResult<TeamItem> getTeams() {
        List<Team> originList = teamRepository.findAll();
        List<TeamItem> result = new ArrayList<>();
        originList.forEach(item -> result.add(new TeamItem.TeamItemBuilder(item).build()));
        return ListConvertService.settingResult(result);
    }

    public void putTeam(long id, TeamUpdateRequest request) {
        Team originData = teamRepository.findById(id).orElseThrow(ClassCastException::new);
        originData.putTeam(request);

        teamRepository.save(originData);
    }
}
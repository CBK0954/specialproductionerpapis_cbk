package shop.dev.choibk.api.member.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import shop.dev.choibk.api.member.entity.Member;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    Optional<Member> findByUsername(String username);
    long countByUsername(String username);
}
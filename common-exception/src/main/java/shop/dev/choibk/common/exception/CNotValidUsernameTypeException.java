package shop.dev.choibk.common.exception;

public class CNotValidUsernameTypeException extends RuntimeException{
    public CNotValidUsernameTypeException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotValidUsernameTypeException(String msg) {
        super(msg);
    }

    public CNotValidUsernameTypeException() {
        super();
    }
}
package shop.dev.choibk.common.exception;

public class CLongUsernameException extends RuntimeException {
    public CLongUsernameException(String msg, Throwable t) {
        super(msg, t);
    }

    public CLongUsernameException(String msg) {
        super(msg);
    }

    public CLongUsernameException() {
        super();
    }
}
package shop.dev.choibk.api.point.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import shop.dev.choibk.api.point.lib.RestApi;
import shop.dev.choibk.api.point.model.MemberProfileResponse;
import shop.dev.choibk.common.exception.CMissingDataException;

@Service
public class MemberInfoService {
    @Value("${api.host.resource.member}")
    private String API_HOST_RESOURCE_MEMBER;

    public MemberProfileResponse getProfile() {
        String apiUrl = this.API_HOST_RESOURCE_MEMBER + "/v1/member/profile";
        try {
            return new RestApi().getApiGetJsonResponse(apiUrl, MemberProfileResponse.class);
        } catch (Exception e) {
            throw new CMissingDataException();
        }
    }
}
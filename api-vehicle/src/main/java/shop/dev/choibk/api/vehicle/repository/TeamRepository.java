package shop.dev.choibk.api.vehicle.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import shop.dev.choibk.api.vehicle.entity.Team;

public interface TeamRepository extends JpaRepository<Team, Long> {

}
package shop.dev.choibk.api.vehicle.model;


import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.dev.choibk.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginResponse {
    @ApiModelProperty(notes = "토큰")
    private String token;

    @ApiModelProperty(notes = "아이디")
    private String username;

    private LoginResponse(LoginResponseBuilder builder) {
        this.token = builder.token;
        this.username = builder.username;
    }

    public static class LoginResponseBuilder implements CommonModelBuilder<LoginResponse> {
        private final String token;
        private final String username;

        public LoginResponseBuilder(String token, String username) {
            this.token = token;
            this.username = username;
        }

        @Override
        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }
}
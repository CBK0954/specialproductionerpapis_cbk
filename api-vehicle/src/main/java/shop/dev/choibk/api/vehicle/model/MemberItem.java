package shop.dev.choibk.api.vehicle.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.dev.choibk.api.vehicle.entity.Member;
import shop.dev.choibk.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {
    private Long id;
    private String username;
    private String password;
    private Boolean isUse;

    private MemberItem(MemberItemBuilder builder) {
        this.username = builder.username;
        this.password = builder.password;
        this.isUse = builder.isUse;
    }
    public static class MemberItemBuilder implements CommonModelBuilder<MemberItem> {
        private final String username;
        private final String password;
        private final Boolean isUse;

        public MemberItemBuilder(Member member) {
            this.username = member.getUsername();
            this.password = member.getPassword();
            this.isUse = member.getIsUse();
        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}
package shop.dev.choibk.api.vehicle.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.dev.choibk.api.vehicle.entity.Team;
import shop.dev.choibk.common.interfaces.CommonModelBuilder;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class TeamItem {
    private String teamName;
    private String mainWork;
    private String teamLeader;
    private Boolean isUse;
    private LocalDateTime createDate;

    private TeamItem(TeamItemBuilder builder) {
        this.teamName = builder.teamName;
        this.mainWork = builder.mainWork;
        this.teamLeader = builder.teamLeader;
        this.isUse = builder.isUse;
        this.createDate = builder.createDate;
    }

    public static class TeamItemBuilder implements CommonModelBuilder<TeamItem> {
        private final String teamName;
        private final String mainWork;
        private final String teamLeader;
        private final Boolean isUse;
        private final LocalDateTime createDate;

        public TeamItemBuilder(Team team) {
            this.teamName = team.getTeamName();
            this.mainWork = team.getMainWork();
            this.teamLeader = team.getTeamLeader();
            this.isUse = team.getIsUse();
            this.createDate = team.getCreateDate();
        }
        @Override
        public TeamItem build() {
            return new TeamItem(this);
        }
    }
}
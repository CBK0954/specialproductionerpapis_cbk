package shop.dev.choibk.api.vehicle.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import shop.dev.choibk.api.vehicle.entity.Member;
import shop.dev.choibk.api.vehicle.entity.Team;
import shop.dev.choibk.api.vehicle.model.MemberDetails;
import shop.dev.choibk.api.vehicle.model.MemberRequest;
import shop.dev.choibk.api.vehicle.model.MemberUpdateRequest;
import shop.dev.choibk.api.vehicle.repository.MemberRepository;
import shop.dev.choibk.api.vehicle.repository.TeamRepository;
import shop.dev.choibk.common.response.model.ListResult;
import shop.dev.choibk.common.response.service.ListConvertService;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;
    private final TeamRepository teamRepository;

    public void setMember(MemberRequest request, Long teamId) {
        Team team = teamRepository.findById(teamId).orElseThrow();
        Member addData = new Member.MemberBuilder(request).setTeam(team).build();

        memberRepository.save(addData);
    }

    public ListResult<MemberDetails> getMembers() {
        List<Member> originList = memberRepository.findAll();
        List<MemberDetails> result = new ArrayList<>();
        originList.forEach(item -> result.add(new MemberDetails.MemberDetailsBuilder(item).build()));
        return ListConvertService.settingResult(result);
    }

    public void putBoard(long id, MemberUpdateRequest request) {
        Member originData = memberRepository.findById(id).orElseThrow(ClassCastException::new);
        originData.putMember(request);

        memberRepository.save(originData);
    }
}
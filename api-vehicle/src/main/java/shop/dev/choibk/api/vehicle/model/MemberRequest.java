package shop.dev.choibk.api.vehicle.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import shop.dev.choibk.common.enums.MemberGroup;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberRequest {
    @ApiModelProperty(value = "아이디", required = true)
    @NotNull
    private String username;

    @ApiModelProperty(value = "비밀번호", required = true)
    private String password;

    @ApiModelProperty(notes = "비밀번호 확인", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String passwordRe;

    @ApiModelProperty(value = "권한", required = true)
    private MemberGroup memberGroup;
}
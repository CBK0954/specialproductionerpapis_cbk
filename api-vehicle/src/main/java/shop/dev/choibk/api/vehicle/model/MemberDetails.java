package shop.dev.choibk.api.vehicle.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import shop.dev.choibk.api.vehicle.entity.Member;
import shop.dev.choibk.common.enums.MemberGroup;
import shop.dev.choibk.common.interfaces.CommonModelBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberDetails {
    private String username;
    private String teamName;
    private MemberGroup memberGroup;

    private MemberDetails(MemberDetailsBuilder builder) {
        this.username = builder.username;
        this.teamName = builder.teamName;
        this.memberGroup = builder.memberGroup;
    }

    public static class MemberDetailsBuilder implements CommonModelBuilder<MemberDetails> {
        private final String username;
        private String teamName;
        private final MemberGroup memberGroup;

        public MemberDetailsBuilder(Member member) {
            this.username = member.getUsername();
            this.memberGroup = member.getMemberGroup();

            if(member.getTeam() != null) {
                this.teamName = member.getTeam().getTeamName();
            }
        }
        @Override
        public MemberDetails build() {
            return new MemberDetails(this);
        }
    }
}
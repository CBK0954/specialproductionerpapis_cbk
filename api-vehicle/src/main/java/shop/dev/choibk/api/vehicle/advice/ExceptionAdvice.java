package shop.dev.choibk.api.vehicle.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import shop.dev.choibk.common.enums.ResultCode;
import shop.dev.choibk.common.exception.*;
import shop.dev.choibk.common.response.model.CommonResult;
import shop.dev.choibk.common.response.service.ResponseService;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CAccessDeniedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CAccessDeniedException e) {
        return ResponseService.getFailResult(ResultCode.ACCESS_DENIED);
    }

    @ExceptionHandler(CAuthenticationEntryPointException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    protected CommonResult customException(HttpServletRequest request, CAuthenticationEntryPointException e) {
        return ResponseService.getFailResult(ResultCode.AUTHENTICATION_ENTRY_POINT);
    }

    @ExceptionHandler(CUsernameSignInFailedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CUsernameSignInFailedException e) {
        return ResponseService.getFailResult(ResultCode.USERNAME_SIGN_IN_FAILED);
    }


    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CWrongPhoneNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWrongPhoneNumberException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_PHONE_NUMBER);
    }

    @ExceptionHandler(CLongUsernameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CLongUsernameException e) {
        return ResponseService.getFailResult(ResultCode.TOO_LONG_USERNAME);
    }

    @ExceptionHandler(CDuplicationUsernameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDuplicationUsernameException e) {
        return ResponseService.getFailResult(ResultCode.DUPLICATION_USERNAME);
    }
    @ExceptionHandler(CWrongPasswordException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CWrongPasswordException e) {
        return ResponseService.getFailResult(ResultCode.WRONG_PASSWORD);
    }

    @ExceptionHandler(CNotValidUsernameTypeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNotValidUsernameTypeException e) {
        return ResponseService.getFailResult(ResultCode.NOT_VALID_USERNAME_TYPE);
    }

}
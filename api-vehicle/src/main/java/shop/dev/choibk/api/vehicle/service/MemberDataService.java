package shop.dev.choibk.api.vehicle.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import shop.dev.choibk.api.vehicle.entity.Member;
import shop.dev.choibk.api.vehicle.entity.Team;
import shop.dev.choibk.api.vehicle.model.MemberRequest;
import shop.dev.choibk.api.vehicle.model.PasswordChangeRequest;
import shop.dev.choibk.api.vehicle.repository.MemberRepository;
import shop.dev.choibk.api.vehicle.repository.TeamRepository;
import shop.dev.choibk.common.enums.MemberGroup;
import shop.dev.choibk.common.exception.*;
import shop.dev.choibk.common.function.CommonCheck;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final MemberRepository memberRepository;
    private final TeamRepository teamRepository;
    private final PasswordEncoder passwordEncoder;


    public void setFirstMember() {
        String username = "superadmin";
        String password = "abcd1234";
        boolean isSuperAdmin = isNewUsername(username);

        if (isSuperAdmin) {
            MemberRequest createRequest = new MemberRequest();
            createRequest.setUsername(username);
            createRequest.setPassword(password);
            createRequest.setPasswordRe(password);
            createRequest.setMemberGroup(MemberGroup.ROLE_ADMIN);

            setMember(createRequest);
        }
    }

    public void setMember(MemberRequest request) {
        if (!CommonCheck.checkUsername(request.getUsername()))
            throw new CNotValidUsernameTypeException(); // 유효한 아이디 형식이 아닙니다
        if (!request.getPassword().equals(request.getPasswordRe()))
            throw new CWrongPasswordException(); // 비밀번호가 일치하지 않습니다
        if (!isNewUsername(request.getUsername())) throw new CUsernameSignInFailedException(); // 아이디 생성에 실패하였습니다

        request.setPassword(passwordEncoder.encode(request.getPassword()));

        Member member = new Member.MemberBuilder(request).build();
        memberRepository.save(member);
    }

    public void setMember(MemberRequest request, long teamId) {
        if (!CommonCheck.checkUsername(request.getUsername()))
            throw new CLongUsernameException(); // 아이디는 20글자 이하로 생성이 가능합니다
        if (!request.getPassword().equals(request.getPasswordRe()))
            throw new CWrongPasswordException(); // 비밀번호가 일치하지 않습니다
        if (!isNewUsername(request.getUsername())) throw new CDuplicationUsernameException(); // 중복된 아이디가 존재합니다

        Team team = teamRepository.findById(teamId).orElseThrow(CMissingDataException::new);

        request.setPassword(passwordEncoder.encode(request.getPassword()));

        Member member = new Member.MemberBuilder(request).setTeam(team).build();
        memberRepository.save(member);
    }

    private boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);
        return dupCount <= 0;
    }

    public void putPassword(long memberId, PasswordChangeRequest changeRequest, boolean isAdmin) {
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);

        if (!isAdmin && !passwordEncoder.matches(changeRequest.getChangePassword(), member.getPassword()))
            throw new CWrongPasswordException(); // 비밀번호가 일치하지 않습니다

        String passwordResult = passwordEncoder.encode(changeRequest.getChangePassword());
        member.putPassword(passwordResult);
        memberRepository.save(member);
    }
}
package shop.dev.choibk.api.vehicle.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import shop.dev.choibk.api.vehicle.model.MemberRequest;
import shop.dev.choibk.api.vehicle.model.MemberUpdateRequest;
import shop.dev.choibk.api.vehicle.service.MemberDataService;
import shop.dev.choibk.api.vehicle.service.MemberService;
import shop.dev.choibk.common.response.model.CommonResult;
import shop.dev.choibk.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "사용자 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")

public class MemberController {
    private final MemberService memberService;
    private final MemberDataService memberDataService;

    @ApiOperation(value = "사용자 등록")
    @PostMapping("/register/{team}")
    public CommonResult setMember(@PathVariable Long team, @RequestBody @Valid MemberRequest request) {
        memberDataService.setMember(request, team);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "사용자 상세정보")
    @GetMapping("/list")
    public CommonResult getMembers() {
        return ResponseService.getListResult(memberService.getMembers(),true);
    }

    @ApiOperation(value = "사용자 수정")
    @PutMapping("/correction/{id}")
    public CommonResult putMember(@PathVariable long id, @RequestBody @Valid MemberUpdateRequest request) {
        memberService.putBoard(id, request);
        return ResponseService.getSuccessResult();
    }
}

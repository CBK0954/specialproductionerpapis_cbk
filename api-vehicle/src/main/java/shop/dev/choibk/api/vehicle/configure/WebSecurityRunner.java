package shop.dev.choibk.api.vehicle.configure;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import shop.dev.choibk.api.vehicle.service.MemberDataService;

@Component
@RequiredArgsConstructor
public class WebSecurityRunner implements ApplicationRunner {
    private final MemberDataService memberDataService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        memberDataService.setFirstMember();
    }
}
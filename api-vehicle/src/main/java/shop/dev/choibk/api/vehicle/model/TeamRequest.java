package shop.dev.choibk.api.vehicle.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class TeamRequest {
    @ApiModelProperty(value = "팀 명", required = true)
    private String teamName;

    @ApiModelProperty(value = "주 업무", required = true)
    private String mainWork;

    @ApiModelProperty(value = "팀장", required = true)
    private String teamLeader;

    @ApiModelProperty(value = "활성화 여부", required = true)
    private Boolean isUse;

    @ApiModelProperty(value = "등록 일자", required = true)
    private LocalDateTime createDate;
}
package shop.dev.choibk.api.vehicle.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberUpdateRequest {
    @NotNull
    private Boolean isUse;
}

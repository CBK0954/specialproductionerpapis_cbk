package shop.dev.choibk.api.vehicle.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import shop.dev.choibk.api.vehicle.model.PasswordChangeRequest;
import shop.dev.choibk.api.vehicle.service.MemberDataService;
import shop.dev.choibk.common.response.model.CommonResult;
import shop.dev.choibk.common.response.service.ResponseService;

import javax.validation.Valid;

@Api(tags = "사용자 비밀번호 수정")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberDataController {
    private final MemberDataService memberDataService;
    @ApiOperation(value = "비밀번호 수정")
    @PutMapping("/change/{id}")
    public CommonResult putPassword(@PathVariable long id, @RequestBody @Valid PasswordChangeRequest changeRequest) {
        memberDataService.putPassword(id, changeRequest, false);
        return ResponseService.getSuccessResult();
    }
}
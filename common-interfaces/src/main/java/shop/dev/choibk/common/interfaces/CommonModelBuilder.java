package shop.dev.choibk.common.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}